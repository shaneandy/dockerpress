// import external dependencies
import 'jquery';
import 'bootstrap/dist/js/bootstrap';

// import local dependencies
import Router from './util/router';
import common from './routes/Common';
import home from './routes/Home';
import aboutUs from './routes/About';

const routes = {
  common,
  home,
  aboutUs,
};

// Load Events
jQuery(document).ready(() => new Router(routes).loadEvents());
