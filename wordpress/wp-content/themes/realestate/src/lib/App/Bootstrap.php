<?php

  namespace App;

  class Bootstrap
  {

    /**
     * Store a 'loaded' flag, in case we are generating this class more than once.
     */
    static private $loaded = false;


    /**
     * Bootstrap App
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 0.0.0
     */

    public function __construct()
    {

      // check if we already loaded the app before.
      if ( static::$loaded ) {
        return;
      }

      // set loaded flag to true so we don't load the app twice
      static::$loaded = true;

      /**
       * Instantiate all app classes
       */
      new CustomTaxonomies();
      new CustomPostTypes();
      new Post();
      new Options();
      // new Setup();
      // new Menu();
      // new Media();
      // new GravityForms();

    } /* __construct() */

  }/* class Bootstrap */
