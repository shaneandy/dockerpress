<?php

  namespace App;

  class Template
  {
    /**
     * Default location of templates
     */
    static private $viewsDirname = 'elements';


    /**
     * Render templates parts using the timber method, but allowing us to not specify a file extension
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function render( $view, $context = array() )
    {
      if ( empty( $view ) )
        return;

      if ( strpos( $view, '.php' ) === false )
        $view .= '.php';

      if ( !file_exists( get_stylesheet_directory() . '/' . static::$viewsDirname . '/' . $view ) )
        return;

      global $app_vars, $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

      if ( is_array( $wp_query->query_vars ) )
        extract( $wp_query->query_vars, EXTR_SKIP );

      // set context in global variable
      $app_vars = $context;

      // require template
      require( get_stylesheet_directory() . '/' . static::$viewsDirname . '/' . $view );


    }/* render */

  }/* class Template */
