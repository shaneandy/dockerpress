<?php

  namespace App;

  class Search
  {

    public function __construct()
    {
    }/* __construct() */


    /**
     * Get the search form for the business page
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getSearchForm( $inHeader = false )
    {

      $searchTerm = self::getSearchQuery();
      $url = get_home_url();

      // <input type="submit" id="searchsubmit" class="btn btn-default " type="button" value="" />

      $form = '<form role="search" method="get" id="searchform" class="searchform icon icon-arrow-right-chevron" action="' . esc_url( home_url( '/search/' ) ) . '">
                  <input type="hidden" name="post_type" value="business" />
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button class="btn btn-default glyphicon glyphicon-search" type="button"></button>
                    </span>
                    <input class="form-control" type="text" placeholder="' . stripcslashes($searchTerm) . '" name="s_term" id="s_term" />
                  </div>
              </form>';

      return $form;

    } /* getSearchForm() */


    /**
     * Get the search form for the blog page
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getBlogSearchForm( $inHeader = false )
    {

      $searchTerm = self::getSearchQuery();

      $form = '<form role="search" method="get" id="searchform" class="searchform icon icon-arrow-right-chevron" action="' . esc_url( home_url( '/search/' ) ) . '">
                  <input type="hidden" name="post_type" value="post" />
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button class="btn btn-default glyphicon glyphicon-search" type="button"></button>
                    </span>
                    <input class="form-control" type="text" placeholder="' . stripcslashes($searchTerm) . '" name="s_term" id="s_term" />
                  </div>
              </form>';

      return $form;

    } /* getBlogSearchForm() */


    /**
     * Get the search query
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getSearchQuery( $placeholder = '' )
    {

      if ( !isset( $_REQUEST[ 's_term' ] ) || empty( $_REQUEST[ 's_term' ] ) )
      {
        if ( empty( $placeholder ) )
          return 'Search';
        else
          return $placeholder;
      }

      return $_REQUEST[ 's_term' ];

    } /* getSearchQuery() */


    /**
     * get search results for global search
     *
     * @author Andrew Vivash
     * @package Search.php
     * @since 1.0
     * @param
     * @return $data
     */

    static public function getSearchResults()
    {

      $postType = !empty($_REQUEST[ 'post_type' ]) ? $_REQUEST['post_type'] : 'business';
      $search = !empty($_REQUEST[ 's_term' ]) ? $_REQUEST['s_term'] : false;
      $perPage = !empty($_REQUEST['perPage']) ? $_REQUEST['perPage'] : 8;
      $pageNum = !empty($_REQUEST['pagenum']) ? $_REQUEST['pagenum'] : 1;

      $body = array(
        'perPage' => $perPage,
        'pagenum' => $pageNum,
      );

      $offset = Utils::getOffsetFromBody( $body );

      $postTypes = array($postType);

      $searchTerm = $search ? urldecode(stripcslashes($search)) : false;

      if (!$searchTerm) {
        return;
      }
      $searchTermArray = explode(" ", $searchTerm);

      $args = array(
        'posts_per_page' => -1,
        'post_type' => $postTypes,
        's' => count($searchTermArray) > 1 ? $searchTermArray : $searchTerm,
      );

      $query = new \WP_Query( $args );

      if ( $query->found_posts == 0 )
      {
        $data = array(
          'message' => 'No results for ' . $search
        );

        return $data;
      }

      $data = array();

      if( $query->have_posts() ) :
        while ( $query->have_posts() ) :

          $query->the_post();

          $result = array();
          $postID = $query->post->ID;
          $postType = get_post_type( $postID );

          switch ( $postType )
          {

            case 'post':

              $result = Post::getDataForPost( $postID );
              break;

            default:
              break;
          }

          $data[ 'posts' ][] = $result;

        endwhile;

        $data[ 'pagination' ] = Utils::getPagination( $query );

        wp_reset_postdata();

      endif;

      return $data;

    }/* getSearchResults() */


  }/* class Search */


