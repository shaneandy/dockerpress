<?php

  namespace App;

  class Options
  {

    public function __construct()
    {

      add_action( 'init', array( &$this, 'init__setupOptions' ) );

    }/* __construct() */


    /**
     * Google Maps API Key
     *
     * @author Andrew Vivash
     * @package Options.php
     */

    static public $googleMapsAPIKey = "AIzaSyD00YhdjJR4ppOKPP-mi2BU77qRj5_y0J4";


    /**
     * initialize the options page
     *
     * @author Andrew Vivash
     * @package Options.php
     */

    public function init__setupOptions(  )
    {

      if (function_exists('acf_add_options_page')) {
        acf_add_options_page( 'Global Settings' );
      }

    }/* init__setupOptions() */

  }/* class Options */
