<?php

  namespace App;

  class Setup
  {
    
    public function __construct()
    {

      add_filter( 'acf/settings/google_api_key', array( &$this, 'init__addACFGoogleMapsKey' ) );    
    
    }/* __construct() */

    
    /**
     * The new version ACF has a disconnect between itself and the google maps API used
     * to display maps in the location custom field within the CMS
     *
     * @author Andrew Vivash
     * @package Setup.php
     */

    public function init__addACFGoogleMapsKey(  )
    {
      
      return Options::$googleMapsAPIKey;
    
    }/* init__addACFGoogleMapsKey() */

  }/* class Setup */
