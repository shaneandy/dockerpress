<?php

  namespace App;

  class CustomTaxonomies
  {

    public function __construct()
    {

      // // register event type
      // add_action( 'init', array( &$this, 'init__registerEventType' ) );

    }/* __construct() */


    /**
     * register event types
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @package CustomTaxonomies
     * @since 1.0
     * @param
     * @return
     */

    public function init__registerEventType()
    {

      $labels = array(
        'name'                  => __( 'Types', 'Taxonomy plural name' ),
        'singular_name'         => __( 'Type', 'Taxonomy singular name' ),
        'search_items'          => __( 'Search Types' ),
        'popular_items'         => __( 'Popular Types' ),
        'all_items'             => __( 'All Types' ),
        'parent_item'           => __( 'Parent Type' ),
        'parent_item_colon'     => __( 'Parent Type' ),
        'edit_item'             => __( 'Edit Type' ),
        'update_item'           => __( 'Update Type' ),
        'add_new_item'          => __( 'Add New Type' ),
        'new_item_name'         => __( 'New Type Name' ),
        'add_or_remove_items'   => __( 'Add or remove Types' ),
        'choose_from_most_used' => __( 'Choose from most used' ),
        'menu_name'             => __( 'Types' ),
      );

      $args = array(
        'labels'            => $labels,
        'public'            => true,
        'show_admin_column' => true,
        'hierarchical'      => true,
        'show_ui'           => true,
        'query_var'         => true,
        'rewrite'           => array(
          'slug' => 'event-type'
        ),
      );

      register_taxonomy( 'event_type', array( 'event' ), $args );

    }/* init__registerEventType() */


  }/* class CustomTaxonomies */
