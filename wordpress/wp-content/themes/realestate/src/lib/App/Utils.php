<?php

  namespace App;

  class Utils
  {
    private function __construct(){}

    /**
     * Test static variable
     */
    static public $var = true;

    /**
     * Utilize Wordpress's default oembed function to embed videos
     *
     * @author Andrew Vivash <@andrewvivash>
     * @package Utils.php
     * @since 1.0
     * @param $url, $width = 0, $height = 0, $post_id = 0
     * @return shortcode
     */

    static public function embed_video_url( $url, $width = 0, $height = 0, $post_id = 0 )
    {

      $wpembed = new \WP_Embed();

      if( !empty( $post_id ) )
        $wpembed->post_ID = $post_id;

      return $wpembed->shortcode( array( 'width' => $width, 'height' => $height ), $url );

    }/* embed_video_url() */


    /**
     * Get the URI to the assets folder in the theme
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getThemeAssetsUri()
    {

      return get_stylesheet_directory_uri() . '/assets/';

    } /* getThemeAssetsUri() */


    /**
     * Get the Path to the assets folder in the theme
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getThemeAssetsPath()
    {

      return get_stylesheet_directory() . '/assets/';

    } /* getThemeAssetsPath() */

    /**
     * Check to see if a variable is set or is empty
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function checkAppVars( $field = '' )
    {

      // Return if no data has been passed
      if ( !isset( $field ) || empty( $field ) )
        return '';


      // We are checking a field within $app_vars by default
      global $app_vars;


      // If data is present, use it, otherwise send back an empty string
      if ( isset( $app_vars[ $field ] ) && !empty( $app_vars[ $field ] ) )
        $data = $app_vars[ $field ];
      else
        $data = '';


      // Return the data to our template
      return $data;


    } /* checkAppVars() */



    /**
     * Check to see if a variable is set or is empty
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function checkIfSet( $dataToCheck = false )
    {

      // Return if no data has been passed
      if ( !$dataToCheck || !isset( $dataToCheck ) || empty( $dataToCheck ) )
        return false;


      if ( isset( $dataToCheck ) && !empty( $dataToCheck ) )
        $data = true;
      else
        $data = false;


      // Return the result
      return $data;

    } /* checkIfSet() */


    /**
     * Helper method to handle a file upload (rather than an upload from a URL)
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @package Module.php
     * @since 1.0
     * @param (array) $file - the $_FILES array from the upload
     * @return (int) $newMediaID - the ID of the full media upload
     */

    static public function handleFileUpload( $file )
    {

      // We need this to handle file uploads
      if( !function_exists( 'wp_handle_upload' ) )
        require_once( ABSPATH . 'wp-admin/includes/file.php' );

      // Get the file object
      if ( $_FILES && isset( $_FILES['profile_image'] ) && !empty( $_FILES['profile_image'] ) )
        $uploadedfile = $_FILES['profile_image'];
      else
        return;

      // WP needs this to actually upload the file
      $upload_overrides = array( 'test_form' => false );

      // move the file to the uploads dir
      $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
      $movefile = self::exif( $movefile );

      if( $movefile )
      {
        $type = $movefile['type'];
        $file = $movefile['file'];
        $url = $movefile['url'];

        $wp_filetype = wp_check_filetype( basename( $file ), null );
        $wp_upload_dir = wp_upload_dir();

        $attachment = array(
          'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
          'post_mime_type' => $wp_filetype['type'],
          'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
          'post_content' => '',
          'post_status' => 'inherit'
        );

        $newMediaID = wp_insert_attachment( $attachment, $file, null );

        // you must first include the image.php file
        // for the function wp_generate_attachment_metadata() to work
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attach_data = wp_generate_attachment_metadata( $newMediaID, $file );
        wp_update_attachment_metadata( $newMediaID, $attach_data );

        // return the original media ID
        return $newMediaID;

      }

    }/* handleFileUpload() */



    /**
     * Handle the image upload using Imagick if available, and ensure
     * we're getting the correct orientation
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function exif( $file )
    {
      if( !$file )
        return $file;

      //This line reads the EXIF data and passes it into an array
      $exif = read_exif_data($file['file']);

      //We're only interested in the orientation
      $exif_orient = isset($exif['Orientation'])?$exif['Orientation']:0;
      $rotateImage = 0;

      //We convert the exif rotation to degrees for further use
      if (6 == $exif_orient)
      {
        $rotateImage = 90;
        $imageOrientation = 1;
      }
      elseif (3 == $exif_orient)
      {
        $rotateImage = 180;
        $imageOrientation = 1;
      }
      elseif (8 == $exif_orient)
      {
        $rotateImage = 270;
        $imageOrientation = 1;
      }

      //if the image is rotated
      if ($rotateImage) {

        //WordPress 3.5+ have started using Imagick, if it is available since there is a noticeable difference in quality
        //Why spoil beautiful images by rotating them with GD, if the user has Imagick

        if (class_exists('Imagick'))
        {
          $imagick = new Imagick();
          $imagick->readImage($file['file']);
          $imagick->rotateImage(new ImagickPixel(), $rotateImage);
          $imagick->setImageOrientation($imageOrientation);
          $imagick->writeImage($file['file']);
          $imagick->clear();
          $imagick->destroy();
        }
        else
        {

          //if no Imagick, fallback to GD
          //GD needs negative degrees
          $rotateImage = -$rotateImage;

          switch ($file['type']) {
            case 'image/jpeg':
              $source = imagecreatefromjpeg($file['file']);
              $rotate = imagerotate($source, $rotateImage, 0);
              imagejpeg($rotate, $file['file']);
              break;
            case 'image/png':
              $source = imagecreatefrompng($file['file']);
              $rotate = imagerotate($source, $rotateImage, 0);
              imagepng($rotate, $file['file']);
              break;
            case 'image/gif':
              $source = imagecreatefromgif($file['file']);
              $rotate = imagerotate($source, $rotateImage, 0);
              imagegif($rotate, $file['file']);
              break;
            default:
              break;
          }
        }
      }
      // The image orientation is fixed, pass it back for further processing
      return $file;
    }


    /**
     * get image based on the passed image ID
     *
     * @author Andrew Vivash
     * @package Utils.php
     * @since 1.0
     * @param $id, $size = ''
     * @return $image
     */

    static public function getImageFromID( $id, $size )
    {

      $imageID = $id;
      $imageObj= wp_get_attachment_image_src( $imageID, $size );
      $image = $imageObj[0];

      return $image;

    }/* getImageFromID() */


    /**
     * get page id based on the template
     *
     * @author Andrew Vivash
     * @package Utils.php
     * @since 1.0
     * @param $template = ''
     * @return $data
     */

    static public function getPageIDFromTemplate( $template = '' )
    {

      if ( empty( $template ) )
        return;

      $query = new \WP_Query(array(
        'post_type'  => 'page',
        'meta_key'   => '_wp_page_template',
        'meta_value' => 'templates/' . $template . '.php'
      ));

      if ( $query->found_posts === 0 )
        return;

      $id = $query->post->ID;

      return $id;

    }/* getPageIDFromTemplate() */


    /**
     * get page ID form path
     *
     * @author Andrew Vivash
     * @package Post.php
     * @since 1.0
     * @param $slug = ''
     * @return $data
     */

    static public function getPageDataFromSlug( $slug = '', $type = '', $postType = '' )
    {

      if ( empty( $slug ) )
        return;

      if ( empty( $postType ) )
        $postType = 'page';

      $page = get_page_by_path( $slug, OBJECT, $postType );

      if ( empty( $page ) )
        return;

      switch ( $type )
      {
        case 'title':
          $data = $page->post_title;
          break;

        case 'slug':
          $data = $page->post_name;
          break;

        case 'id':
          $data = $page->ID;
          break;

        default:
          break;
      }


      return $data;

    }/* getPageDataFromSlug() */


    /**
     * get the slug based on the post ID
     *
     * @author Andrew Vivash
     * @package Utils.php
     * @since 1.0
     * @param $postID
     * @return $data
     */

    static public function getSlugFromID( $postID )
    {
      if ( gettype( $postID ) == 'string' )
        // make sure that the ID is an integer
        $postID = intval( $postID );

      // get the post based on the ID
      // then get the slug
      $post = get_post( $postID );
      $slug = $post->post_name;

      return $slug;

    }/* getSlugFromID() */


    /**
     * Get the id for an attachment from the image src
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string)  $url  - Description
     * @return (object) Description
     */

    static public function getAttachmentIDFromSrc( $url )
    {

      // Split the $url into two parts with the wp-content directory as the separator
      $parsedUrl  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );


      // Get the host of the current site and the host of the $url, ignoring www
      $thisHost = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
      $fileHost = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );


      // Return nothing if there aren't any $url parts or if the current host and $url host do not match
      // if ( ! isset( $parsedUrl[1] ) || empty( $parsedUrl[1] ) || ( $thisHost != $fileHost ) )
      if ( ! isset( $parsedUrl[1] ) || empty( $parsedUrl[1] ) )
        return;

      // Now we're going to quickly search the DB for any attachment GUID with a partial path match
      // Example: /uploads/2013/05/test-image.jpg
      global $wpdb;

      $attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsedUrl[1] ) );

      // Returns null if no attachment is found
      return $attachment[0];

    } /* getAttachmentIDFromSrc() */



    /**
     * Helper method to determine the offset from a request body
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @package Utils.php
     * @since 1.0
     * @param (array) $body - the request body
     * @return (int) $offset - the offset for the loop
     */

    static public function getOffsetFromBody( $body )
    {

      // See if we have a posts_per_page amount, default 10
      $postsPerPage = ( isset( $body['perPage'] ) && !empty( $body[ 'perPage' ] ) ) ? $body['perPage'] : 10;

      // See if we have a page number request, default 1
      $page = ( isset( $body['pagenum'] ) && !empty( $body[ 'pagenum' ] ) ) ? $body['pagenum'] : 1;
      // If we have a page that isn't 1, then we need to calculate an offset, otherwise it's 0. This will break default pagination.
      $offset = ( $page != 1 ) ? ( $page - 1 ) * $postsPerPage : 0;

      return $offset;

    }/* getOffsetFromBody() */


    /**
     * get posts pagination
     *
     * @author Andrew Vivash
     * @package Publication.php
     * @since 1.0
     * @param $query
     * @return $data
     */

    static public function getPagination( $query )
    {

      if ( empty( $query ) )
        return;

      $data = array();

      if ( isset( $_REQUEST['pagenum'] ) )
        $paged = $_REQUEST['pagenum'];
      else if ( get_query_var( 'paged' ) )
        $paged = get_query_var( 'paged' );
      else
        $paged = 1;

      $data = Utils::customPagination( $query->max_num_pages, 1, $paged );

      return $data;

    }/* getPagination() */



    /**
     * Custom pagination for the stories archive
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function customPagination( $numpages = '', $pagerange = '', $currentPage = '' )
    {


      if ( empty( $pagerange ) )
        $pagerange = 2;

      /**
       * This first part of our function is a fallback
       * for custom pagination inside a regular loop that
       * uses the global $paged and global $wp_query variables.
       *
       * It's good because we can now override default pagination
       * in our theme, and use this function in default quries
       * and custom queries.
       */
      global $paged;

      if ( empty( $currentPage ) )
        $paged = 1;
      else
        $paged = $currentPage;


      if ( $numpages == '' )
      {
        global $wp_query;

        $numpages = $wp_query->max_num_pages;
        if( !$numpages )
        {
            $numpages = 1;
        }

      }


      /**
       * We construct the pagination arguments to enter into our paginate_links
       * function.
       */
      $args = array(
        // 'base'            => get_pagenum_link(1) . '%_%',
        'format'          => '?pagenum=%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => false,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => true,
        'prev_text'       => 'Previous',
        'next_text'       => 'Next',
        'type'            => 'list',
        'add_args'        => array(),
        'add_fragment'    => '',
      );

      $paginateLinks = paginate_links( $args );

      if ( $paginateLinks )
        return $paginateLinks;

    } /* customPagination() */

  }/* class Utils */
